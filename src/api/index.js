import axios from 'axios';

let protocol = 'http';
let url = '127.0.0.1';
let port = '8080';

const API = axios.create({
  baseURL: protocol + '://' + url + ':' + port + '/',
});

export default API;
